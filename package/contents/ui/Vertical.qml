/*
 *   Copyright 2012 Alex Merry <alex.merry@kdemail.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import "../code/service.js" as Control

Grid {
    id: root

    property int minimumWidth: playPauseButton.minimumWidth
    property int minimumHeight: (playPauseButton.minimumHeight * (albumArt.visible ? 4 : 3)) / columns

    Component.onCompleted: {
        plasmoid.addEventListener('ConfigChanged', function(){
            albumArt.visible = plasmoid.readConfig("displayCover");
        });
    }

    flow: Grid.TopToBottom
    columns: (width > 2*playPauseButton.minimumWidth) ? 2 : 1

    AlbumArt {
        id: albumArt
        height: (visible ? width : 0)
        width: (root.width / columns)
    }
    // HACK: PlasmaComponents.ToolButton works on a width-for-height basis,
    //       so we have to explicitly set the buttons to be square
    BackButton {
        width: (root.width / columns)
        height: width
    }
    PlayPauseButton {
        id: playPauseButton
        width: (root.width / columns)
        height: width
    }
    NextButton {
        width: (root.width / columns)
        height: width
    }
}

// vi:sts=4:sw=4:et
