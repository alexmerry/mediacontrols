/*
 *   Copyright 2012 Alex Merry <alex.merry@kdemail.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import "../code/service.js" as Control

Grid {
    id: root

    property int minimumWidth: (playPauseButton.minimumWidth * (albumArt.visible ? 4 : 3)) / rows
    property int minimumHeight: playPauseButton.minimumHeight

    Component.onCompleted: {
        plasmoid.addEventListener('ConfigChanged', function(){
            albumArt.visible = plasmoid.readConfig("displayCover");
        });
    }

    flow: Grid.TopToBottom
    rows: (height > 2*playPauseButton.minimumHeight) ? 2 : 1

    AlbumArt {
        id: albumArt
        width: (visible ? height : 0)
        height: (root.height / rows)
    }
    BackButton {
        height: (root.height / rows)
    }
    PlayPauseButton {
        id: playPauseButton
        height: (root.height / rows)
    }
    NextButton {
        height: (root.height / rows)
    }
}

// vi:sts=4:sw=4:et
